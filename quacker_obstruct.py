import io
import string

reserved = """
abstract	arguments	await	boolean
break	byte	case	catch
char	class	const	continue
debugger	default	delete	do
double	else	enum	eval
export	extends	false	final
finally	float	for	function
goto	if	implements	import
in	instanceof	int	interface
let	long	native	new
null	package	private	protected
public	return	short	static
super	switch	synchronized	this
throw	throws	transient	true
try	typeof	var	void
volatile	while	with	yield
""".split()

objects = """
Array	Date	eval	function
hasOwnProperty	Infinity	isFinite	isNaN
isPrototypeOf	length	Math	NaN
name	Number	Object	prototype
String	toString	undefined	valueOf
alert	all	anchor	anchors
area	assign	blur	button
checkbox	clearInterval	clearTimeout	clientInformation
close	closed	confirm	constructor
crypto	decodeURI	decodeURIComponent	defaultStatus
document	element	elements	embed
embeds	encodeURI	encodeURIComponent	escape
event	fileUpload	focus	form
forms	frame	innerHeight	innerWidth
layer	layers	link	location
mimeTypes	navigate	navigator	frames
frameRate	hidden	history	image
images	offscreenBuffering	open	opener
option	outerHeight	outerWidth	packages
pageXOffset	pageYOffset	parent	parseFloat
parseInt	password	pkcs11	plugin
prompt	propertyIsEnum	radio	reset
screenX	screenY	scroll	secure
select	self	setInterval	setTimeout
status	submit	taint	text
textarea	top	unescape	untaint
window splice console
""".split()

class MiniObstructor():
    def __init__(self, assign=True):
        self.i = 0
        self.output = []
        self.var_names = {}
        self.assign = assign
        self.alphabet = string.ascii_lowercase + string.ascii_uppercase

    def toString(self):
        return "".join(self.output)

    def namespace(self, string):
        m = MiniObstructor(assign=False)
        m.var_names = self.var_names
        m.feed(io.StringIO(string))
        return "".join(m.output)

    def get_forwards_identifier(self, buffer):
        self.skip_spaces(buffer)
        build = ""
        c = buffer.read(1)
        while c and (c.isalpha() or c.isdigit() or c == '_'):
            build += c
            c = buffer.read(1)
        if buffer.read(1):
            buffer.seek(buffer.tell()-2)
        return build

    def skip_spaces(self, buffer):
        c = buffer.read(1)
        while c and not c.strip():
            c = buffer.read(1)
        buffer.seek(buffer.tell()-1)

    def assign_identifier(self, identifier):
        if not identifier or identifier.isdigit(): return identifier
        if identifier in reserved or identifier in objects: return identifier
        if identifier in self.var_names: return self.var_names[identifier]
        if not self.assign: return identifier
        new = self.alphabet[self.i]
        self.i += 1
        self.var_names[identifier] = new
        return new

    def _buffer_from(self, identifier):
        if not identifier or identifier.isdigit(): return identifier
        buffer = io.StringIO(identifier)
        new = self.get_forwards_identifier(buffer)
        b = buffer.read()
        if b.startswith('[') and b.endswith(']'):
            b = '[' + self._buffer_from(b[1:-1]) + ']'
        return self.assign_identifier(new) + b

    def feed(self, buffer):
        build = ""
        c = buffer.read(1)
        while c:
            if c == "\"" or c == "'":
                _string = ""
                c2 = buffer.read(1)
                while c2 and c2 != c:
                    _string += c2
                    c2 = buffer.read(1)
                self.output.append(repr(_string))
            elif build.strip() in ["var", "return", "typeof"]:
                self.output.append(build+" ")
                build = ""
            elif c == ".":
                identifier = self._buffer_from(build)
                identifier2 = self.get_forwards_identifier(buffer)
                if identifier:
                    self.output.extend([identifier])
                self.output.append(c)
                if identifier2:
                    if identifier2 in self.var_names:
                        self.output.append(self.assign_identifier(identifier2))
                    else:
                        self.output.append(identifier2)
                build = ""
            elif c != "_" and c in string.punctuation: #[":", "=", ",", ";"]:
                identifier = self._buffer_from(build)
                #print(c, "C", build, "B")
                #print(repr(build), identifier, "bi")
                if identifier:
                    self.output.append(identifier)
                elif build:
                    self.output.append(build)
                self.output.append(c)
                build = ""
            elif c == ")":
                identifier = self._buffer_from(build)
                if identifier:
                    self.output.append(identifier)
                elif build:
                    self.output.append(build)
                build = ""
                self.output.append(c)
            elif c == "(":
                if build:
                    self.output.append(build)
                self.output.append(c)
                build = ""
            elif c in ["{", "}"]:
                identifier = self._buffer_from(build)
                if identifier:
                    self.output.append(identifier)
                    build = ""
                elif build:
                    self.output.append(build)
                    build = ""
                self.output.append(c)
            elif c == '\n':
                c = self.output[-1]
                if c not in "{},;":
                    self.output.append(";")
            elif c.strip():
                build += c
            elif build == "else":
                build += c
            c = buffer.read(1)
        self.output.append(build)
        return build
    

if __name__ == '__main__':
    mObstruct = MiniObstructor()
    mObstruct.feed(open("render.js", "r"))
    print(mObstruct.var_names)
    #print(mObstruct.output)
    open("render.min.js", "w").write("".join(mObstruct.output))