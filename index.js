function Toolbar() {
    return (
        QuackerDOM.create('nav', {'class': 'navbar navbar-expand-lg navbar-light bg-light'}, QuackerDOM.create('a', {'class': 'navbar-brand', 'href': '#'}, 'Navbar'), QuackerDOM.create('button', {'class': 'navbar-toggler', 'type': 'button', 'data-toggle': 'collapse', 'data-target': '#navbarSupportedContent', 'aria-controls': 'navbarSupportedContent', 'aria-expanded': 'false', 'aria-label': 'Toggle navigation'}, QuackerDOM.create('span', {'class': 'navbar-toggler-icon'})), QuackerDOM.create('div', {'class': 'collapse navbar-collapse', 'id': 'navbarSupportedContent'}, QuackerDOM.create('ul', {'class': 'navbar-nav mr-auto'}, QuackerDOM.create('li', {'class': 'nav-item active'}, QuackerDOM.create('a', {'class': 'nav-link', 'href': '#'}, 'Home ', QuackerDOM.create('span', {'class': 'sr-only'}, '(current)'))), QuackerDOM.create('li', {'class': 'nav-item'}, QuackerDOM.create('a', {'class': 'nav-link', 'href': '#'}, 'Link')), QuackerDOM.create('li', {'class': 'nav-item dropdown'}, QuackerDOM.create('a', {'class': 'nav-link dropdown-toggle', 'href': '#', 'id': 'navbarDropdown', 'role': 'button', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false'}, 'Dropdown'), QuackerDOM.create('div', {'class': 'dropdown-menu', 'aria-labelledby': 'navbarDropdown'}, QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Action'), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Another action'), QuackerDOM.create('div', {'class': 'dropdown-divider'}), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Something else here'))), QuackerDOM.create('li', {'class': 'nav-item'}, QuackerDOM.create('a', {'class': 'nav-link disabled', 'href': '#'}, 'Disabled'))), QuackerDOM.create('form', {'class': 'form-inline my-2 my-lg-0'}, QuackerDOM.create('input', {'class': 'form-control mr-sm-2', 'type': 'search', 'placeholder': 'Search', 'aria-label': 'Search'}), QuackerDOM.create('button', {'class': 'btn btn-outline-success my-2 my-sm-0', 'type': 'submit'}, 'Search'))))
    );
}
function Container() {
    return (
        QuackerDOM.create('div', {'class': 'container'}, 'Some text on this Page.')
    );
}
function CreateList(props) {
    props.className = props.className ? props.className : "list-group-item";
    return (
        props.list.map((text) => QuackerDOM.create('li', {'class': props.className}, QuackerDOM.create('input', {'name': text.toLowerCase(), 'class': 'form-control', 'value': text})))
    );
}
function Sortable(props) {
    var s = (QuackerDOM.create('ul', {'class': props.dropSpace ? 'dropSpace' : ''+' list-group connectedSortable'}, CreateList({list: props.list, className: "list-group-item ui-state-default"})));
    return $(s).sortable({connectWith: ".connectedSortable"}).disableSelection()[0];
}
function Toolbox() {
    return (
        QuackerDOM.create('div', {'class': 'row'}, QuackerDOM.create('div', {'class': 'col-sm-4'}, Sortable({list: ["1", "2"], dropSpace: true})), QuackerDOM.create('div', {'class': 'col-sm-4'}, Sortable({list: ["First", "Second", "Third"]}), QuackerDOM.create('div', null, QuackerDOM.state.first)))
    );
}
function Root() {
    return (
        QuackerDOM.create('div', null, Toolbar(), QuackerDOM.create('div', {'class': 'container'}, Container(), ' ', Toolbox(), ' '))
    );
}
QuackerDOM.render(Root(), document.querySelector('#root'));function UserComment(props) {
    return (
        QuackerDOM.create('form', {'class': 'card-link', 'role': 'form', 'action': '/add_comment', 'method': 'POST'}, QuackerDOM.create('input', {'type': 'hidden', 'name': 'postid', 'value': props.post_id}), QuackerDOM.create('div', {'class': 'input-group mb-3'}, QuackerDOM.create('input', {'name': 'text', 'type': 'text', 'class': 'form-control', 'placeholder': 'Reply...', 'aria-label': 'Reply...', 'aria-describedby': 'basic-addon2'}), QuackerDOM.create('div', {'class': 'input-group-append'}, QuackerDOM.create('input', {'id': 'basic-addon2', 'type': 'submit', 'class': 'btn', 'value': 'Comment'}))))
    );
}
function PublicComment(props) {
    return (QuackerDOM.create('p', {'class': 'comment mb-2 row bg-light p-2 border-left border-primary'}, QuackerDOM.create('a', {'href': Flask.url_for('login')}, 'Sign in'), '\xa0to leave a comment.'));
}
function ActionComment(props) {
    return (
        QuackerDOM.create('div', {'class': 'card-footer'}, Globals.user ? UserComment(props) : PublicComment(props))
    );
}
/**/
function Post(props) {
    return (
        QuackerDOM.create('div', {'id': 'post-template', 'class': 'card gedf-card mb-4'}, QuackerDOM.create('div', {'class': 'card-header'}, QuackerDOM.create('div', {'class': 'd-flex justify-content-between align-items-center'}, QuackerDOM.create('div', {'class': 'd-flex justify-content-between align-items-center'}, QuackerDOM.create('div', {'class': 'mr-2'}, QuackerDOM.create('img', {'class': 'rounded-circle', 'width': '45', 'src': Flask.url_for('show_gravatar', {email: props.email, s: 48}), 'alt': props.first_name+"'s Icon"})), QuackerDOM.create('div', {'class': 'ml-2'}, QuackerDOM.create('div', {'class': 'h5 m-0'}, QuackerDOM.create('a', {'href': Flask.url_for('user_messageboard', {user_id: props.user_id})}, props.first_name, ' ', props.last_name, ' ')), QuackerDOM.create('div', {'class': 'h7 text-muted'}, Formats.datetime(props.pub_date)))), QuackerDOM.create('div', null, QuackerDOM.create('div', {'class': 'dropdown'}, QuackerDOM.create('button', {'class': 'btn btn-link dropdown-toggle', 'type': 'button', 'id': 'gedf-drop1', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false'}, QuackerDOM.create('i', {'class': 'fa fa-ellipsis-h'})), QuackerDOM.create('div', {'class': 'dropdown-menu dropdown-menu-right', 'aria-labelledby': 'gedf-drop1'}, QuackerDOM.create('div', {'class': 'h6 dropdown-header'}, 'Configuration'), QuackerDOM.create('div', {'class': 'post-user-actions'}, QuackerDOM.create('a', {'class': 'dropdown-item', 'href': Flask.url_for('get_post', {post_id: props.post_id, edit: 'true'})}, QuackerDOM.create('i', {'class': 'far fa-edit'}), ' Edit'), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': 'javascript:delete_post('+props.post_id+'javascript:delete_post()'}, QuackerDOM.create('i', {'class': 'far fa-trash-alt'}), ' Delete')), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Save'), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Hide'), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Report')))))), QuackerDOM.create('div', {'class': 'card-body'}, QuackerDOM.create('div', {'class': 'text-muted h7 mb-2'}, QuackerDOM.create('i', {'class': 'far fa-clock'}), ' ', Formats.messagetime(props.pub_date), ' '), QuackerDOM.create('a', {'class': 'card-link', 'href': Flask.url_for('get_post', {post_id: props.post_id})}, QuackerDOM.create('h5', {'class': 'card-title'}, props.title)), QuackerDOM.create('p', {'class': 'card-text'}, QuackerHTML(props.text)), QuackerDOM.create('div', {'class': 'post-user-group'}, QuackerDOM.create('a', {'href': Flask.url_for('group_messageboard', {id: props.class_id})}, QuackerDOM.create('span', {'class': 'badge badge-primary'}, QuackerDOM.create('i', {'class': 'far fa-eye'}), props.class_name))), QuackerDOM.create('hr', null), QuackerDOM.create('div', {'class': 'comment_frame'})), ActionComment(props))
    );
}
function PostGroup(props) {
    return (
        QuackerDOM.create('div', {'class': 'card-group'}, props.posts.map((post) => Post({class_name: "x", pub_date: "0", text: "text", first_name: "Daniel", last_name: "Mitchell", title: post.title})))
    );
}
function Container() {
    return (
        QuackerDOM.create('div', {'class': 'container'}, QuackerDOM.create('div', {'class': 'input-group'}, QuackerDOM.create('input', {'name': 'q', 'class': 'form-control'}), QuackerDOM.create('select', {'name': 'nomen', 'class': 'form-control'}, QuackerDOM.create('option', {'value': 'apples'}, 'Apples'), QuackerDOM.create('option', {'value': 'oranges'}, 'Oranges'), QuackerDOM.create('option', {'value': 'bananas'}, 'Bananas')), QuackerDOM.create('button', {'class': 'btn btn-primary', 'onclick': "alert([a.b.q, a.b.nomen.toLowerCase()].join(', '));"}, 'Search')), PostGroup({posts: Posts}))
    );
}
/*QuackerDOM.render(Container(), document.querySelector('#root'));*/