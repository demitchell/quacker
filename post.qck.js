function UserComment(props) {
    return (
        QuackerDOM.create('form', {'class': 'card-link', 'role': 'form', 'action': '/add_comment', 'method': 'POST'}, QuackerDOM.create('input', {'type': 'hidden', 'name': 'post_id', 'value': props.post_id}), QuackerDOM.create('div', {'class': 'input-group mb-3'}, QuackerDOM.create('input', {'name': 'text', 'type': 'text', 'class': 'form-control', 'placeholder': 'Reply...', 'aria-label': 'Reply...', 'aria-describedby': 'basic-addon2'}), QuackerDOM.create('div', {'class': 'input-group-append'}, QuackerDOM.create('input', {'id': 'basic-addon2', 'type': 'submit', 'class': 'btn', 'value': 'Comment'}))))
    );
}
function PublicComment(props) {
    return (QuackerDOM.create('p', {'class': 'comment mb-2 row bg-light p-2 border-left border-primary'}, QuackerDOM.create('a', {'href': Flask.url_for('login')}, 'Sign in'), '\xa0to leave a comment.'));
}
function ActionComment(props) {
    return (
        QuackerDOM.create('div', {'class': 'card-footer'}, Globals.user ? UserComment(props) : PublicComment(props))
    );
}
function CommentActions(props) {
    return (
        <div>
            <a href="{Flask.url_for('get_comment', {post_id: props.post_id, edit: 'true'})}" class="text-right small"><i class="fas fa-pencil-alt"></i> Edit</a> | <a href="javascript:delete_post('{props.post_id}')" class="text-right small"><i class="fas fa-trash-alt"></i> Delete</a>
        </div>
    );
}
function Comment(props) {
    return (
    <div class="comment ml-2 row border-top pt-2">
        <div class="comment-avatar">
            <a href=""><img class="mx-auto rounded-circle img-fluid" width="45" src="{Flask.url_for('show_gravatar', {email: props.email, s: 48})}" alt="avatar" /></a>
        </div>
        <div class="comment-content ml-3">
            <h6 class="small comment-meta"><a href="{Flask.url_for('user_messageboard', {user_id: props.user_id})}">{props.first_name} {props.last_name}</a> <small class="text-muted ml-2"><span class="far fa-clock"></span> {Formats.messagetime(props.pub_date)}</small></h6>
            <div class="comment-body">
                <p>
                    {QuackerHTML(props.text)}
                    <br/>
                    {props.user_id == Globals.user_id ? CommentActions(props) : ""}
                </p>
            </div>
        </div>
    </div>);
}
function CommentLoader(props){
    return (props.len > (props.page+1)*props.per_page) ? (<button class="btn btn-link commentLoadBtn" onclick="load_comments($(this).parent(), {props.parent_id}, {(props.page+1)});"><i class="fas fa-sync"></i> Load More</button>) : "";
}
function CommentLabel(props){
    return props.len ? (<h3 class="comment-lbl">({props.len}) Comment{(props.len > 1) ? "s" : ""}:</h3>) : "";
}
function Post(props) {
    return (
        QuackerDOM.create('div', {'id': 'post-template', 'class': 'card gedf-card mb-4'}, QuackerDOM.create('div', {'class': 'card-header'}, QuackerDOM.create('div', {'class': 'd-flex justify-content-between align-items-center'}, QuackerDOM.create('div', {'class': 'd-flex justify-content-between align-items-center'}, QuackerDOM.create('div', {'class': 'mr-2'}, QuackerDOM.create('img', {'class': 'rounded-circle', 'width': '45', 'src': Flask.url_for('show_gravatar', {email: props.email, s: 48}), 'alt': props.first_name+"'s Icon"})), QuackerDOM.create('div', {'class': 'ml-2'}, QuackerDOM.create('div', {'class': 'h5 m-0'}, QuackerDOM.create('a', {'href': Flask.url_for('user_messageboard', {user_id: props.user_id})}, props.first_name, ' ', props.last_name, ' ')), QuackerDOM.create('div', {'class': 'h7 text-muted'}, Formats.datetime(props.pub_date)))), QuackerDOM.create('div', null, QuackerDOM.create('div', {'class': 'dropdown'}, QuackerDOM.create('button', {'class': 'btn btn-link dropdown-toggle', 'type': 'button', 'id': 'gedf-drop1', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false'}, QuackerDOM.create('i', {'class': 'fa fa-ellipsis-h'})), QuackerDOM.create('div', {'class': 'dropdown-menu dropdown-menu-right', 'aria-labelledby': 'gedf-drop1'}, QuackerDOM.create('div', {'class': 'h6 dropdown-header'}, 'Configuration'), props.user_id == Globals.user_id ? QuackerDOM.create('div', {'class': 'post-user-actions'}, QuackerDOM.create('a', {'class': 'dropdown-item', 'href': Flask.url_for('get_post', {post_id: props.post_id, edit: 'true'})}, QuackerDOM.create('i', {'class': 'far fa-edit'}), ' Edit'), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': 'javascript:delete_post('+props.post_id+')'}, QuackerDOM.create('i', {'class': 'far fa-trash-alt'}), ' Delete')) : "", QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Save'), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Hide'), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Report')))))), QuackerDOM.create('div', {'class': 'card-body'}, QuackerDOM.create('div', {'class': 'text-muted h7 mb-2'}, QuackerDOM.create('i', {'class': 'far fa-clock'}), ' ', Formats.messagetime(props.pub_date), ' '), QuackerDOM.create('a', {'class': 'card-link', 'href': Flask.url_for('get_post', {post_id: props.post_id})}, QuackerDOM.create('h5', {'class': 'card-title'}, props.title)), QuackerDOM.create('p', {'class': 'card-text'}, QuackerHTML(props.text)), (props.formatting == 1) ? QuackerDOM.create('div', {'class': 'post-user-group'}, props.class_id ? QuackerDOM.create('a', {'href': Flask.url_for('group_messageboard', {id: props.class_id})}, QuackerDOM.create('span', {'class': 'badge badge-primary'}, QuackerDOM.create('i', {'class': 'far fa-eye'}), " ", props.class_name)): " ") : "", QuackerDOM.create('hr', null), QuackerDOM.create('div', {'class': 'comment_frame'})), ActionComment(props))
    );
}
function PostGroup(props) {
    return (
        QuackerDOM.create('div', {'class': 'card-group'}, props.posts.map((post) => Post({class_name: "x", pub_date: "0", text: "text", first_name: "Daniel", last_name: "Mitchell", title: post.title})))
    );
}
function Container() {
    return (
        QuackerDOM.create('div', {'class': 'container'}, QuackerDOM.create('div', {'class': 'input-group'}, QuackerDOM.create('input', {'name': 'q', 'class': 'form-control'}), QuackerDOM.create('select', {'name': 'nomen', 'class': 'form-control'}, QuackerDOM.create('option', {'value': 'apples'}, 'Apples'), QuackerDOM.create('option', {'value': 'oranges'}, 'Oranges'), QuackerDOM.create('option', {'value': 'bananas'}, 'Bananas')), QuackerDOM.create('button', {'class': 'btn btn-primary', 'onclick': "alert([a.b.q, a.b.nomen.toLowerCase()].join(', '));"}, 'Search')), PostGroup({posts: Posts}))
    );
}
function load_comments(commentFrame, parent_id, page, per_page) {
    page = page ? page : 0;
    per_page = per_page ? per_page : 3;
    $.getJSON(Flask.url_for('json_get_posts', {parent: parent_id, cp: [page, per_page].join(".")}), function (data) {
        commentFrame.find(".comment-lbl").remove();
        commentFrame.find(".commentLoadBtn").remove();
        commentFrame.prepend(CommentLabel(data));
        data.posts.forEach((comment, idx) => {
        commentFrame.append(Comment(comment));
        });
        // QuackerDOM.expire(QuackerDOM.state.changed(commentFrame, 'content'))
        data = Object.assign({}, data, {page: page, per_page: per_page, parent_id: parent_id});
        commentFrame.append(CommentLoader(data));
    });
}
/*QuackerDOM.render(Container(), document.querySelector('#root'));*/