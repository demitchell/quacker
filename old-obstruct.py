def change_name(self, m):
    c = self.alphabet[self.i]
    pre, identifier = m.group(1), m.group(2)
    sign = m.group(4)
    if pre and pre.endswith('.') and pre[:-1] not in self.var_names: return pre+identifier+sign
    if identifier in ["function", "if"]: return identifier + sign
    if sign in '.)':
        if identifier in self.var_names:
            return self.var_names[identifier] + sign
        return identifier + sign
    char = ' ' if sign == '=' else ''
    pre = pre+char if pre else char
    self.var_names[identifier] = c
    self.i += 1
    return pre + c + sign
def include_quacker2(minify=False):
    import re
    rep = lambda m: r"\2" if minify else r"\1"
    mObstruct = MiniObstructor()
    return re.sub(r"(var|[A-Za-z0-9_]+\.)?\s?([A-Za-z0-9_]+)(\s)?(\:|\=|\.|\(|\))(\s)?", mObstruct.change_name, open("render.js", "r").read())
