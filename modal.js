function Toolbar() {
    return (
        QuackerDOM.create('nav', {'class': 'navbar navbar-expand-lg navbar-light bg-light'}, QuackerDOM.create('a', {'class': 'navbar-brand', 'href': '#'}, QuackerDOM.create('h3', null, 'Quacker.js')), QuackerDOM.create('button', {'class': 'navbar-toggler', 'type': 'button', 'data-toggle': 'collapse', 'data-target': '#navbarSupportedContent', 'aria-controls': 'navbarSupportedContent', 'aria-expanded': 'false', 'aria-label': 'Toggle navigation'}, QuackerDOM.create('span', {'class': 'navbar-toggler-icon'})), QuackerDOM.create('div', {'class': 'collapse navbar-collapse', 'id': 'navbarSupportedContent'}, QuackerDOM.create('ul', {'class': 'navbar-nav mr-auto'}, QuackerDOM.create('li', {'class': 'nav-item active'}, QuackerDOM.create('a', {'class': 'nav-link', 'href': '#'}, 'Home ', QuackerDOM.create('span', {'class': 'sr-only'}, '(current)'))), QuackerDOM.create('li', {'class': 'nav-item'}, QuackerDOM.create('a', {'class': 'nav-link', 'href': '#'}, 'Link')), QuackerDOM.create('li', {'class': 'nav-item dropdown'}, QuackerDOM.create('a', {'class': 'nav-link dropdown-toggle', 'href': '#', 'id': 'navbarDropdown', 'role': 'button', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false'}, 'Dropdown'), QuackerDOM.create('div', {'class': 'dropdown-menu', 'aria-labelledby': 'navbarDropdown'}, QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Action'), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Another action'), QuackerDOM.create('div', {'class': 'dropdown-divider'}), QuackerDOM.create('a', {'class': 'dropdown-item', 'href': '#'}, 'Something else here'))), QuackerDOM.create('li', {'class': 'nav-item'}, QuackerDOM.create('a', {'class': 'nav-link disabled', 'href': '#'}, 'Disabled'))), QuackerDOM.create('form', {'class': 'form-inline my-2 my-lg-0'}, QuackerDOM.create('input', {'class': 'form-control mr-sm-2', 'type': 'search', 'placeholder': 'Search', 'aria-label': 'Search'}), QuackerDOM.create('button', {'class': 'btn btn-outline-success my-2 my-sm-0', 'type': 'submit'}, 'Search'))))
    );
}
function Container() {
    return (
        QuackerDOM.create('div', {'class': 'container'}, 'Some text on this Page.')
    );
}
function CreateList(props) {
    props.className = props.className ? props.className : "list-group-item";
    return (
        props.list.map((text) => QuackerDOM.create('li', {'class': props.className}, QuackerDOM.create('input', {'name': text.toLowerCase(), 'class': QuackerDOM.fromState('inputClass'), 'value': text})))
    );
}
function Sortable(props) {
    var s = (QuackerDOM.create('ul', {'class': props.dropSpace ? 'dropSpace' : ''+' list-group connectedSortable'}, CreateList({list: props.list, className: "list-group-item ui-state-default"})));
    return $(s).sortable({connectWith: ".connectedSortable"}).disableSelection()[0];
}
function Toolbox() {
    return (
        QuackerDOM.create('div', {'class': 'row'}, QuackerDOM.create('div', {'class': 'col-sm-4'}, Sortable({list: ["1", "2"], dropSpace: true})), QuackerDOM.create('div', {'class': 'col-sm-4'}, Sortable({list: ["First", "Second", "Third"]}), QuackerDOM.create('div', null, QuackerDOM.fromState("first")), QuackerDOM.create('input', {'class': 'form-control', 'value': QuackerDOM.fromState('first')})))
    );
}
function Hello(props) {
    return (
        QuackerDOM.create('h1', null, props.input)
    );
}
function HelloParent() {
    return (
        QuackerDOM.create('div', null, ['hello', 'world', 'id', 'apple', 'orange'].map((el) => Hello({input: el})))
    );
}
function Root() {
    return (
        QuackerDOM.create('div', null, Toolbar(), QuackerDOM.create('div', null, QuackerDOM.create('button', {'class': 'btn btn-primary', 'onclick': "QuackerDOM.setState('inputClass', 'form-control')"}, 'Change Class')), QuackerDOM.create('div', {'class': 'container'}, Container(), ' ', Toolbox()), QuackerDOM.create('div', null, HelloParent()))
    );
}
QuackerDOM.render(Root(), document.querySelector('#root'));function Modal(props) {
    // QuackerDOM.create('button', {'type': 'button', 'class': 'btn btn-primary'})
    return (
        QuackerDOM.create('div', {'class': 'modal fade', 'id': props.id ? props.id : 'quackerModal', 'tabindex': '-1', 'role': 'dialog', 'aria-labelledby': 'quackerModalLabel'}, QuackerDOM.create('div', {'class': 'modal-dialog', 'role': 'document'}, QuackerDOM.create('div', {'class': 'modal-content'}, QuackerDOM.create('div', {'class': 'modal-header'}, QuackerDOM.create('h5', {'class': 'modal-title'}, props.title), QuackerDOM.create('button', {'type': 'button', 'class': 'close', 'data-dismiss': 'modal', 'aria-label': 'Close'}, QuackerDOM.create('span', {'aria-hidden': 'true'}, '×'))), QuackerDOM.create('div', {'class': 'modal-body'}, QuackerDOM.create('p', null, props.content)), QuackerDOM.create('div', {'class': 'modal-footer'}, QuackerDOM.create('button', {'type': 'button', 'class': 'btn btn-secondary', 'data-dismiss': 'modal'}, 'Close')))))
    );
}