function Toolbar() {
    return (
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"><h3>Quacker.js</h3></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
    );
}
function Container() {
    return (
        <div class="container">
            Some text on this Page.
        </div>
    );
}
function CreateList(props) {
    props.className = props.className ? props.className : "list-group-item";
    return (
        props.list.map((text) => <li class={props.className}><input name="{text.toLowerCase()}" class="{QuackerDOM.fromState('inputClass')}" value="{text}" /></li>)
    );
}
function Sortable(props) {
    var s = (<ul class="{props.dropSpace ? 'dropSpace' : ''} list-group connectedSortable">
        {CreateList({list: props.list, className: "list-group-item ui-state-default"})}
    </ul>);
    return $(s).sortable({connectWith: ".connectedSortable"}).disableSelection()[0];
}
function Toolbox() {
    return (
        <div class="row">
            <div class="col-sm-4">
                {Sortable({list: ["1", "2"], dropSpace: true})}
            </div>
            <div class="col-sm-4">
                {Sortable({list: ["First", "Second", "Third"]})}
                <div>{QuackerDOM.fromState("first")}</div>
                <input class="form-control" value="{QuackerDOM.fromState('first')}" />
            </div>
        </div>
    );
}
function Hello(props) {
    return (
        <h1>{props.input}</h1>
    );
}
function HelloParent() {
    return (
        <div>
            {['hello', 'world', 'id', 'apple', 'orange'].map((el) => Hello({input: el}))}
        </div>
    );
}
function Root() {
    return (
        <div>{Toolbar()}</div>
        <div><button class="btn btn-primary" onclick="QuackerDOM.setState('inputClass', 'form-control')">Change Class</button></div>
        <div class="container">{Container()} {Toolbox()}</div>
        <div>{HelloParent()}</div>
    );
}
QuackerDOM.render(Root(), document.querySelector('#root'));