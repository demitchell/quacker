var QuackerHTML = function (content) {
    return {content: content, render: function (el) {el.innerHTML += this.content}};
};
var StateValue = function (content) {
    return {value: content, trace: [], parent: []};
};
var QuackerDOM = {
    state: {},
    setState: function (name, val) {
        // sets everything but Parent Source Object
        if(typeof val != "string") {
            var e = name;
            e = e.target ? e.target : e; // can be from event or element
            name = e.getAttribute("name");
            val = e.value;
            val = val ? val : e.innerText;
        }
        if(Object.keys(this.state).includes(name)) {
            // updates state
            var stateEntry = this.state[name];
            stateEntry.value = val;
            stateEntry.trace.forEach(function (el) {
                el.nodeValue = val;
                if (el.nodeType == 2) {
                    el.ownerElement.setAttributeNode(el);
                    if(el.nodeName == "value") el.ownerElement.value = val; // Display active value
                }
            });
        } else {
            // creates state
            this.state[name] = StateValue(val);
        }
    },
    fromState: function (key, attr) {
        var stateEntry = this.state[key];
        if(stateEntry) {
            var textNode = document.createTextNode(stateEntry.value); // how we update text in DOM!
            if (attr) {
                textNode = document.createAttribute(attr);
                textNode.value = stateEntry.value;
            }
            stateEntry.trace.push(textNode);
            textNode._qref = key;
            return textNode;
        }
        var default_val = attr ? attr : "";
        this.setState(key, default_val);
        return this.fromState(key);
    },
    addStateTrace: function (key, node) {
        var stateEntry = this.state[key];
        if(stateEntry) {
            stateEntry.trace.push(node);
        }
        return node;
    },
    removeStateTrace: function (key, node) {
        var stateEntry = this.state[key];
        if(stateEntry) {
            return stateEntry.trace.splice(stateEntry.trace.indexOf(node), 1);
        }
        return [];
    },
    render: function (el, container) {
        container.appendChild(el);
    },
    create: function () {
        var args = Object.values(arguments);
        var children = args.splice(2);
        var tag = args[0], attr = args[1];
        var el = document.createElement(tag);
        if(attr) {
            Object.keys(attr).forEach((key) => {
                var val = attr[key];
                var node = document.createAttribute(key);
                if (val.nodeType) {
                    if (val.nodeType != 2) {
                        var _qref = val._qref;
                        //console.log(_qref);
                        this.removeStateTrace(_qref, val);
                        val = this.fromState(_qref, key);
                    }
                    node = val;
                } else {
                    node.value = attr[key];
                }
                el.setAttributeNode(node);
            });
        }
        if(children) {
            children.forEach((child) => {
                if (Array.isArray(child)) {
                    child.forEach((child2) => el.appendChild(child2));
                } else if (child && child.nodeType) {
                    el.appendChild(child);
                } else if (child && child.render) {
                    child.render(el);
                } else if (child != undefined) {
                    el.appendChild(document.createTextNode(child.toString()));
                }
            });
        }
        if(tag == "input" && attr.name) {
            this.setState(el);
            //console.log("input", attr.name);
            //this.fromState(attr.name, "value");
            this.addStateTrace(attr.name, el.getAttributeNode("value"));
            el.addEventListener("keyup", this.setState.bind(this));
        } else if (tag == "select" && attr.name) {
            this.setState(el);
            this.addStateTrace(attr.name, el.getAttributeNode("value"));
            el.addEventListener("change", this.setState.bind(this));
        }
        return el;
    }
};
var request = {
    qdom: QuackerDOM,
    settings: {
        timeout: 5000
    },
    get: function (url, fn, timeout) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                return fn(xhttp.responseText);
            }
        };
        var bind = this.qdom;
        xhttp.open("GET", url, true);
        //xhr.timeout = 4000;
        xhttp.send();
        return bind;
    },
    getJSON: function (url, fn) {
        this.get(url, function (data) {return fn(JSON.parse(data));});
    }
};