#!/usr/bin/env python3
import os
import sys
import json
import io
from html.parser import HTMLParser
from quacker_obstruct import MiniObstructor

class QuackerDOM():
    def __init__(self, qmaker, tag, attr):
        self.qmaker = qmaker
        self.tag = tag
        attrib = []
        for e in attr:
            k, v = e
            if '{' in v:
                v = "+".join([repr(v) for v in self.qmaker.handle_escapes(v)])
            else:
                v = repr(v)
            attrib.append(": ".join([repr(k), v]))
        self.attr = "{"+", ".join(attrib)+"}" if attr else "null"
        self.elements = []
        self._closed = False

    def toString(self, create_fn="QuackerDOM.create", splitter=", "):
        if self.elements:
            return "%s(%s)" % (create_fn, splitter.join([repr(self.tag), self.attr]+[e.toString(create_fn) if isinstance(e, QuackerDOM) else repr(e) for e in self.elements]))
        return "%s(%s)" % (create_fn, splitter.join([repr(self.tag), self.attr]))
    
    def __repr__(self):
        return "<QuackerDOM %s>" % self.tag

class Escape():
    def __init__(self, text):
        self.text = text
    def __repr__(self):
        return self.text

class QMaker(HTMLParser):
    qelements = []
    customs = {}
    mObstruct = MiniObstructor()
    def __init__(self, minify=False, include=False):
        super().__init__()
        self.minify = minify
        self.include = include
        if self.include:
            if minify:
                self.mObstruct = MiniObstructor()
                self.mObstruct.feed(open("render.js", "r"))
                self.qelements.append(self.mObstruct.toString())
            else:
                self.qelements.append(open("render.js", "r").read())
    def handle_starttag(self, tag, attr):
        self.qelements.append(QuackerDOM(self, tag, attr))
    def handle_data(self, data):
        if data.strip():
            if self.qelements and isinstance(self.qelements[-1], QuackerDOM) and not self.qelements[-1]._closed:
                if '\n' in data:
                    data = data.strip()
                return self.qelements[-1].elements.extend(self.handle_escapes(data))
            if self.minify:
                for d in data.split('\n'):
                    d = d.strip()
                    if '.' in d and self.minify:
                        d = self.mObstruct.namespace(d)
                    self.qelements.append(d)
            else:
                self.qelements.append(data)
    def handle_escapes(self, data):
        escaped = []
        build = ""
        data = io.StringIO(data)
        c = data.read(1)
        while c:
            if c == '{':
                if build: escaped.append(build)
                escaped.append(Escape(self._get_par(data)))
                #print(build, "Bl")
                build = ""
            else:
                build += c
            c = data.read(1)
        if build: escaped.append(build)
        #print(escaped)
        return escaped
    def _get_par(self, data, end='}'):
        build = ""
        c = data.read(1)
        while c:
            if c == '{':
                build += '{' + self._get_par(data) + '}'
            elif c == '}':
                if self.minify:
                    build2 = ""
                    for d in build.split('\n'):
                        build2 += d.strip()
                    return build2
                return build
            else:
                build += c
            c = data.read(1)
        return build
    def handle_endtag(self, tag):
        last_el = self.qelements.pop()
        last_el._closed = True
        if isinstance(self.qelements[-1], QuackerDOM):
            self.qelements[-1].elements.append(last_el)
        else:
            self.qelements.append(last_el)
    def toString(self):
        include = ""
        if self.minify:
            print(self.qelements)
            return "".join([e.toString(self.mObstruct.namespace("QuackerDOM.create"), ",") if isinstance(e, QuackerDOM) else e for e in self.qelements])
        return include + "".join([e.toString() if isinstance(e, QuackerDOM) else e for e in self.qelements])


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Compile QuackerCode to js.')
    parser.add_argument('-i', action="store", dest="input")
    parser.add_argument('-o', action="store", dest="output")
    parser.add_argument('--all', dest='all', action='store_const',
                    const=True, default=False,
                    help='compile all files with "*.qck.js" ending (default: false)')
    parser.add_argument('--min', dest='minify', action='store_const',
                    const=True, default=False,
                    help='compress mode minify (default: false)')
    parser.add_argument('--include', dest='include', action='store_const',
                    const=True, default=False,
                    help='include quacker render in file (default: false)')
    args = parser.parse_args()
    output_func = lambda f: args.output
    if args.output is None:
        output_func = lambda f: f.rsplit('.', 2)[0]+".js"
    files = [args.input] if args.input and not args.all else os.listdir('.')
    for f in files:
        if os.path.isfile(f) and f.endswith('.qck.js'):
            qm = QMaker(minify=args.minify, include=args.include)
            qm.feed(open(f, "r").read())
            print("Writing: ", output_func(f), "\nSize: ", open(output_func(f), "w").write(qm.toString()), "Bytes")